import { IServiceProvider } from "./iServiceProvider";

export interface IServiceScope {

  serviceProvider: IServiceProvider;

}