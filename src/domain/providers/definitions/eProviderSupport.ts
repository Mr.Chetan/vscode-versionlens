export enum ProviderSupport {
  Releases = 'releases',
  Prereleases = 'prereleases'
}