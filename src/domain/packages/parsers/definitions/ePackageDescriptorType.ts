export enum PackageDescriptorType {
  version = "version",
  path = "path",
  git = "git",
  hosted = "hosted"
}