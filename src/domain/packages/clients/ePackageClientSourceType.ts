export enum PackageClientSourceType {
  Directory = 'directory',
  File = 'file',
  Git = 'git',
  Github = 'github',
  Registry = 'registry'
}